# Mock Tracks

## Setup:
```npm install```

## Usage:
```npm start -- --port 3002 --origin http://localhost:3000```

## Debugging:
```npm run debug -- --port 3002 --origin http://localhost:3000```

## Options:
* origin is defaulted to `http://localhost:3000`
* port is defaulted to `3001`
