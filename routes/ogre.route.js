'use strict';

const cloneDeep = require('clone-deep');
let trackRequestCount = 0;
let trackHistory = {
    // <id>: [],
};

module.exports.init = function(app, origin) {
    app.get('/esd', allowCORS, function(req, res, next) {
        const trackData = require('../data/esd2.json');
        const cloned = cloneDeep(trackData);

        trackRequestCount++;
        while(trackRequestCount > 15) {
            trackRequestCount -= 15;
            trackHistory = {};
        }

        updateTracks(cloned.tracks.esd, trackRequestCount);

        return res.json(cloned);
    });

    app.get('/link16', allowCORS, function(req, res, next) {
        const trackData = require('../data/link16.json');
        const cloned = cloneDeep(trackData);

        trackRequestCount++;
        while(trackRequestCount > 15) {
            trackRequestCount -= 15;
            trackHistory = {};
        }

        updateTracks(cloned.tracks.link16, trackRequestCount);

        return res.json(cloned);
    });

    function updateTracks(tracks, count) {
        tracks.forEach(function(track) {
            track = updateTrack(track, count);

            if (!trackHistory[track.id]) {
                trackHistory[track.id] = [];
            }

            trackHistory[track.id].push(track);
            tracks.trackhistory = trackHistory[track.id];
        });
    }

    function updateTrack(track, count) {
        //Modify this to update tracks
        const altDelta = (count % 2) ? -1000 : 1000;
        track.altitude = track.altitude + altDelta;
        track.geometry.coordinates = [
            track.geometry.coordinates[0] + (.1 * count),
            track.geometry.coordinates[1] + (.1 * count)
        ];

        return track;
    }

    function allowCORS(req, res, next) {
        res.header('Access-Control-Allow-Origin', origin);
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type');
        res.header('Access-Control-Allow-Credentials', 'true');

        next();
    }
}
