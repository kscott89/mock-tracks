'use strict';

var express = require('express');
var request = require('request');
var argv = require('yargs').argv;

var port = argv.port || 3001;
var origin = argv.origin || 'http://localhost:3000';
var app = express();

// can be used to serve static data
// app.use(express.static('static'));

require('./routes').init(app, origin);

app.listen(port);
console.log('Now listening on port [%s]', port);

process.on('uncaughtException', function (err) {
    console.log(err);
});
